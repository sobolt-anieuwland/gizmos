# Gizmos
You are viewing Sobolt's Gizmos repository. This repository contains useful, 
single-purpose scripts that automate oft-encountered tasks at Sobolt. 

Please feel free to expand this repository with your own scripts. The only
requirements are that the scripts are (mostly) generic and simple enough to fit
in a single file.

## Current scripts
Below is an alphabetically-ordered list of script names followed by their
function. 

* `get_shp_bounds.py`: Founds the upperleftmost and lowerrightmost corners that describe a square in which all features in the given shape file fall
* `exclude_small_shapes.py`: Removes as many (expressed as a percentage between 0 and 100) shapes in a shapefile below a certain surface area as provided.
* `intersect_bounds_and_shapes.py`: Finds those tiles in the bounding box that intersect with shape from the second shape file.
* `tile_for_shape.py`: Tiles a given tif in a grid-like fashion such that the shape defines the edges of the tiles. Execute without arguments to see command specifics.
