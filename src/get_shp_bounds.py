#!/usr/bin/env python

import fiona
import sys

def execute(shp_path, out_path, out_format):
    bounds = boundaries_from_shapefile(shp_path)
    write(bounds, out_path, out_format)

def write(bounds, out_path, out_format):
    # For now only prints as WKT to terminal
    # In principle would like to support more out formats (shp, geojson, ...)
    bounds_wkt = []
    bounds_wkt.append( str(bounds[0]) + " " + str(bounds[1]) )
    bounds_wkt.append( str(bounds[2]) + " " + str(bounds[1]) )
    bounds_wkt.append( str(bounds[2]) + " " + str(bounds[3]) )
    bounds_wkt.append( str(bounds[0]) + " " + str(bounds[3]) )
    bounds_wkt.append( str(bounds[0]) + " " + str(bounds[1]) )
    bounds_wkt = ", ".join(bounds_wkt)
    print("POLYGON((" + bounds_wkt + "))")

def boundaries_from_shapefile(path):
    # Separate function for opening the file to separate IO concerns from 
    # iteration logic concerns
    fiona_shapefile = fiona.open(path) 
    return boundaries_from_polygons(fiona_shapefile)

def boundaries_from_polygons(fiona_shapefile):
    ulx = None
    uly = None
    lrx = None
    lry = None
    
    for feature in fiona_shapefile:
        for polygon in feature['geometry']['coordinates']:
            for coord in polygon:
                # Is not a coordinate in the shape we expect, ignore
                if len(coord) != 2:
                    continue
                
                # Overwrite existing smallest/largest x/y values if a 
                # smaller or larger one is encountered
                if ulx is None or coord[0] < ulx:
                    ulx = coord[0]
                
                if lrx is None or coord[0] > lrx:
                    lrx = coord[0]
                
                if uly is None or coord[1] < uly:
                    uly = coord[1]
                
                if lry is None or coord[1] > lry:
                    lry = coord[1]
    return (ulx, uly, lrx, lry)


def bounds_from_polygon(polygon):
    xs = []
    ys = []
    
    for coord in polygon:
        if len(coord) != 2:
            continue
        
        xs.append(coord[0])
        ys.append(coord[1])
    
    if len(xs) == 0 or len(ys) == 0:
        return None
    
    return min(xs), max(ys), max(xs), min(ys)


if __name__ == "__main__":
    if len(sys.argv) < 1:
        print("Usage: get_shp_bounds.py <shp_file>")
    else:
        execute(sys.argv[1], None, None)
