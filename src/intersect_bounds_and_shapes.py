#!/usr/bin/env python
import sys

import geopandas as gpd
import numpy as np


def execute(bounds_shp, shapes_shp, out_path, tile_size):
    bounds = gpd.read_file(bounds_shp)
    shapes = gpd.read_file(shapes_shp)
    grid = create_grid(bounds, tile_size)
    print("intersect_bounds_and_shapes: Created grid")
    print("intersect_bounds_and_shapes: Searching for tiles intersecting with shapes")
    
    # Find the tiles that can be removed because they do not intersect with any shape
    for_removal = []
    for idx, tile in grid.iterrows():
        intersects_any_truth_vals = shapes.intersects(tile['geometry'])
        intersects_any = False
        for truth_val in intersects_any_truth_vals:
            intersects_any = intersects_any or truth_val
            if intersects_any:
                break
        
        if not intersects_any:
            for_removal.append(idx)
    
    print("intersect_bounds_and_shapes: Removing unnecessary tile")
    # Remove the identified tiles from the grid
    for idx in for_removal:
        grid.drop(idx, inplace=True)
        
    # Write grid to file
    print("intersect_bounds_and_shapes: Writing out")
    grid.to_file(out_path)


def create_grid(gdf, grid_size = 128,
                crs = {'init':'epsg:28992', 'no_defs':True}):

    x_min, y_min, x_max, y_max = gdf.total_bounds
    width = height = grid_size
    rows = int(np.ceil((y_max-y_min) / height))
    cols = int(np.ceil((x_max-x_min) / width))
    x_left_origin = x_min
    x_right_origin = x_min + width
    y_top_origin = y_max
    y_bottom_origin = y_max - height
    polygons = []
    for i in range(cols):
        y_top = y_top_origin
        y_bottom = y_bottom_origin
        for j in range(rows):
            polygons.append(Polygon([(x_left_origin, y_top), (x_right_origin, y_top),
                    (x_right_origin, y_bottom), (x_left_origin, y_bottom)]))
            y_top = y_top - height
            y_bottom = y_bottom - height
        x_left_origin = x_left_origin + width
        x_right_origin = x_right_origin + width

    grid_gdf = gpd.GeoDataFrame({'geometry':polygons}, crs=crs)
    grid_gdf = intersect_polygons(gdf, grid_gdf)

    return grid_gdf


if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: intersect_bounds_and_shapes.py <bounds shp> <polygon shp> <out> <tile_size>")
        print("Assumes an srs of 28992")
        print("Takes two shape files, the first a bounding box, the second one with many polygons")
        print("Subsequently, this script divides the bounding box into a grid with sizes of <tile_size>.")
        print("Then, those tiles that do not intersect with any of the polygons in the shapefile are")
        print("removed. The result is written out as a shapefile to <out>")
        exit(1)
    else:
        execute(sys.argv[1], sys.argv[2], sys.argv[3], float(sys.argv[4]))
