#!/usr/bin/env python

# Te doen: functie die slechts een percentage verwijdert

import geopandas as gpd
import argparse as ap
import random as rnd
import sys

def execute():
    args = parse_arguments()
    print("Arguments are: " + str(args))
    inshape = gpd.read_file(args.inshape)
    outshape = filter_small_shapes(inshape, args.area, args.pc)
    outshape.to_file(args.outshape)


def filter_small_shapes(inshape, area, pc):
    outshape = inshape.copy()
    outshape.crs = inshape.crs
    
    # If the units of the shapes are not meters, set them to such a crs
    orig_crs = None
    if inshape.crs['units'] != "m":
        orig_crs = inshape.crs
        print("Converting to epsg:32633")
        outshape = outshape.to_crs({"init": "epsg:32633"}) 
    
    # Find the indices of those shapes/geometries that are too small
    droppable_idxs = []
    length = len(outshape.index)
    for idx, row in outshape.iterrows():
        sys.stdout.write("\rFinding shapes that are too small: " + str(idx+1) + " / " + str(length))
        sys.stdout.flush()
        if row['geometry'].area < area:
            droppable_idxs.append(idx)
    print()
    
    # Remove those rows
    for i, idx in enumerate(droppable_idxs):
        sys.stdout.write("\rRemoving those shapes: " + str(i+1) + " / " + str(len(droppable_idxs)))
        if roll_dice(pc):
            outshape.drop(idx, inplace=True)
        sys.stdout.flush()
    print()
    
    # Restore crs if not original
    if orig_crs != None:
        print("Restoring original crs")
        outshape.to_crs(orig_crs)
    
    return outshape # FIXME write with a crs, currently no crs gets configured


def roll_dice(percentage):
    return rnd.randint(0, 100) < percentage


def parse_arguments():
    ds = "Removes a percentage of shapes in a shapefile whose surface area is smaller than the given number"
    parser = ap.ArgumentParser(description=ds)
    parser.add_argument("inshape", type=str, help="Path to the input shape file")
    parser.add_argument("area", type=int, help="The minimal area in m2 a shape must be")
    parser.add_argument("pc", type=is_percentage, help="The percentage to remove")
    parser.add_argument("outshape", type=str, help="Path to the output shape file")
    return parser.parse_args()


def is_percentage(string):
    pc = float(string)
    if pc < 0.0 or pc > 1.0:
        msg = "%r is not a valid percentage between 0 and 1" % string
        raise argparse.ArgumentTypeError(msg)
    return pc * 100

if __name__ == "__main__":
    execute()
    print("Done")
