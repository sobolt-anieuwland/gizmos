#!/usr/bin/env python

def tile_raster(


def create_grid(gdf, grid_size = 128,
                crs = {'init':'epsg:28992', 'no_defs':True}):

    x_min, y_min, x_max, y_max = gdf.total_bounds
    width = height = grid_size
    rows = int(np.ceil((y_max-y_min) / height))
    cols = int(np.ceil((x_max-x_min) / width))
    x_left_origin = x_min
    x_right_origin = x_min + width
    y_top_origin = y_max
    y_bottom_origin = y_max - height
    polygons = []
    for i in range(cols):
        y_top = y_top_origin
        y_bottom = y_bottom_origin
        for j in range(rows):
            polygons.append(Polygon([(x_left_origin, y_top), (x_right_origin, y_top),
                    (x_right_origin, y_bottom), (x_left_origin, y_bottom)]))
            y_top = y_top - height
            y_bottom = y_bottom - height
        x_left_origin = x_left_origin + width
        x_right_origin = x_right_origin + width

    grid_gdf = gpd.GeoDataFrame({'geometry':polygons}, crs=crs)
    grid_gdf = intersect_polygons(gdf, grid_gdf)

    return grid_gdf
