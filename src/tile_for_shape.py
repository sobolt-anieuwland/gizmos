#!/usr/bin/env python
import sys

import geopandas as gpd
import numpy as np
import rasterio as rio
import shapely.wkt as wkt
import os

from rasterio.mask import mask
from shapely.geometry import Polygon


def execute(shp, raster_file, out_path, tmpl, tile_size, threshold = None):
    gdf = gpd.read_file(shp)
    grid = create_grid(gdf, tile_size)
    print("tile_for_shape: Created grid")

    path_imgs = out_path + tmpl + "/"
    path_geoms = out_path + "geoms/"
    make_dir_if_not_exists(path_imgs)
    make_dir_if_not_exists(path_geoms)
    print("tile_for_shape: Created output directories")

    i = 0
    failed = 0
    errors = {}
    print("tile_for_shape: Starting tile generation")
    for shape in grid['geometry']:
        if threshold is not None and i > threshold:
            msg = "tile_for_shape: Exiting because optional parameter to stop at {} was given"
            msg.format(threshold)
            print(msg)
            exit()

        try:
            img, meta = cookie_cutter(raster_file, shape)

            name = str(i+1) + "_" + tmpl
            write_raster(img, meta, path_imgs, name)
            write_geometry(shape, path_geoms, name)
            print("tile_for_shape: Finished tile {}".format(i+1))
        except Exception as e:
            failed += 1
            errors[i] = e
            print(("tile_for_shape: Failed on tile {} because: " + str(e)).format(i))
        i += 1
    if bool(errors):
        print(errors)
    print("tile_for_shape: Processed {} tiles, out of which {} failed".format(i, failed))


def create_grid(gdf, grid_size = 128,
                crs = {'init':'epsg:28992', 'no_defs':True}):

    x_min, y_min, x_max, y_max = gdf.total_bounds
    width = height = grid_size
    rows = int(np.ceil((y_max-y_min) / height))
    cols = int(np.ceil((x_max-x_min) / width))
    x_left_origin = x_min
    x_right_origin = x_min + width
    y_top_origin = y_max
    y_bottom_origin = y_max - height
    polygons = []
    for i in range(cols):
        y_top = y_top_origin
        y_bottom = y_bottom_origin
        for j in range(rows):
            polygons.append(Polygon([(x_left_origin, y_top), (x_right_origin, y_top),
                    (x_right_origin, y_bottom), (x_left_origin, y_bottom)]))
            y_top = y_top - height
            y_bottom = y_bottom - height
        x_left_origin = x_left_origin + width
        x_right_origin = x_right_origin + width

    grid_gdf = gpd.GeoDataFrame({'geometry':polygons}, crs=crs)
    grid_gdf = intersect_polygons(gdf, grid_gdf)

    return grid_gdf


def intersect_polygons(input_gdf, grid_gdf):
    multipolygon = input_gdf.geometry[0]
    subset_gdf = grid_gdf[grid_gdf.intersects(multipolygon)]
    return subset_gdf


def cookie_cutter(raster_file, shape):
    # Cut out part of image
    with rio.open(raster_file) as src:
        out_image, out_transform = mask(src, [shape], invert=False, all_touched=True, crop=True)
        out_meta = src.meta.copy()

    out_meta.update({ "driver":   "PNG",
                      "height":    out_image.shape[1],
                      "width":     out_image.shape[2],
                      "transform": out_transform })
    return out_image, out_meta


def write_raster(img, meta, path, name):
    with rio.open(path + name + ".tif", "w", **meta) as dst:
        dst.write(img)


def write_geometry(geometry, out_path, name):
    filepath = out_path + name + ".txt"
    with open(filepath, 'w') as out:
        out.write("{}".format(geometry))


def make_dir_if_not_exists(path):
    if not os.path.exists(path):
        os.makedirs(path)


if __name__ == "__main__":
    if len(sys.argv) < 6:
        print("Usage: tile_for_shape.py <shp> <tif> <dir> <filename> <tile_size> [<stop_count>]")
        print("Assumes an srs of 28992")
        exit(1)
    else:
        threshold = None
        if len(sys.argv) > 6:
            threshold = int(sys.argv[6])
        execute(sys.argv[1], sys.argv[2], sys.argv[3]+"/", sys.argv[4],
                float(sys.argv[5]), threshold)
